package com.classpath.redisdemo.client;

import com.classpath.redisdemo.model.Order;
import com.classpath.redisdemo.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RedisCrudClient implements CommandLineRunner {

    private final OrderRepository orderRepository;

    @Override
    public void run(String... args) throws Exception {
        Order order = Order.builder().customerName("vinay").price(10000).build();
        this.orderRepository.saveOrder(order);
    }
}
