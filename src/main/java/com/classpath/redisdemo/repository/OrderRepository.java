package com.classpath.redisdemo.repository;

import com.classpath.redisdemo.model.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Set;

@Repository
@RequiredArgsConstructor
public class OrderRepository {

    private final String hashReference= "orders";

    private final RedisTemplate redisTemplate;

    public void saveOrder(Order order){
        this.redisTemplate.persist(order);
    }


}
